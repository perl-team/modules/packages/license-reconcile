Source: license-reconcile
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: gregor herrmann <gregoa@debian.org>
Section: devel
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: dh-sequence-bash-completion,
               debhelper-compat (= 12),
               libmodule-build-perl,
               perl
Build-Depends-Indep: libclass-xsaccessor-perl <!nocheck>,
                     libconfig-any-perl <!nocheck>,
                     libdebian-copyright-perl <!nocheck>,
                     libdpkg-perl <!nocheck>,
                     libemail-address-xs-perl <!nocheck>,
                     libfile-fnmatch-perl <!nocheck>,
                     libfile-mmagic-perl <!nocheck>,
                     libfile-slurp-perl <!nocheck>,
                     liblist-moreutils-perl <!nocheck>,
                     libparse-debianchangelog-perl <!nocheck>,
                     libreadonly-perl,
                     libset-intspan-perl <!nocheck>,
                     libsmart-comments-perl <!nocheck>,
                     libsoftware-licensemoreutils-perl <!nocheck>,
                     libtest-compile-perl <!nocheck>,
                     libtest-deep-perl <!nocheck>,
                     libtest-exception-perl <!nocheck>,
                     libtest-nowarnings-perl <!nocheck>,
                     libtest-output-perl <!nocheck>,
                     libtext-levenshteinxs-perl <!nocheck>,
                     libuniversal-require-perl <!nocheck>,
                     libyaml-libyaml-perl <!nocheck>,
                     licensecheck <!nocheck>
Standards-Version: 4.4.0
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/license-reconcile
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/license-reconcile.git

Package: license-reconcile
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends},
         libclass-xsaccessor-perl,
         libconfig-any-perl,
         libdebian-copyright-perl,
         libdpkg-perl,
         libemail-address-xs-perl,
         libfile-fnmatch-perl,
         libfile-mmagic-perl,
         libfile-slurp-perl,
         liblist-moreutils-perl,
         libparse-debianchangelog-perl,
         libreadonly-perl,
         libset-intspan-perl,
         libsmart-comments-perl,
         libtext-levenshteinxs-perl,
         libuniversal-require-perl,
         licensecheck
Recommends: libyaml-libyaml-perl
Description: tool to reconcile copyright file and source
 Out of the box the license-reconcile tool compares licensecheck output
 and the debian/changelog file against the debian/copyright file. However
 the power of the tool is that the behaviour can be overridden and complemented
 by rules. Rules include the ability to file match, to match against
 licensecheck output and to extract copyright years. The rules can be defined
 in a file and should need to be changed less often than the debian/copyright
 file itself. If necessary copyright and license data can be extracted
 from the source code in more specialized ways by adding Perl modules
 below Debian::LicenseReconcile::Filter.
