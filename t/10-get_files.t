#!/usr/bin/perl

use Test::More tests => 1;
use Debian::LicenseReconcile::Utils qw(get_files);
use Test::Deep;

my @files = sort { $a cmp $b} get_files('t/data/example');
cmp_deeply(\@files, [
	'a/0.h',
	'a/1.h',
	'a/2.h',
	'a/3.h',
	'a/base',
	'a/g/blah',
	'a/g/scriggs.t',
	'a/scriggs.g',
	'base',
	'base.h',
	'debian/changelog',
	'debian/control',
	'debian/copyright',
        'sample.png',
]);
